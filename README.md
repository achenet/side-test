# Side Technical Test - Back end

### Creating and running the database

Use the following set of commands to create a Docker image of the database and run it:

```sh
docker build -t sidedb db/
docker run -d --name sidedb sidedb
docker start sidedb
  
```

Verify that the db container is running with `docker ps`.

It is possible to connect to the database once it is running with

To find the IP address of the database, use the command
```sh
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' sidedb

```
If the IP address is stored in the environment variable `DB_IP`, then it is possible to connect to the database with the command

```sh
psql -h $DB_IP -p 5432 -U postgres
```

and by entering the password `toto` when prompted.

### Startup
The app can be run with `go run main.go`

#### Flags
The flag `--connection-string` should specify the connection string for the database.
By default, this will be `postgres://postgres:toto@172.17.0.2:5432`.

It should have the form `postgres://postgres:toto@$DB_URL:5432`.
