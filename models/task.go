package models

import (
	"errors"
	"fmt"
	"side-test/db"
)

type Task struct {
	ID   string `json:"id" db:"id"`
	Name string `json:"name" db:"name"`
	//	Applicants uint
	Status   string `json:"status" db:"status"`
	Assignee string `json:"assigneeId" db:"assignee"`

	Organisation `json:"organisation"`
	Slots        `json:"slots"`
}

const (
	UPCOMING = "Upcoming"
	ONGOING  = "Ongoing"
	DONE     = "Done"
)

type Organisation struct {
	ID         string `json:"id"`
	Name       string `json:"name"`
	PictureURL string `json:"pictureUrl"`
}

type Slots struct {
	Filled uint `json:"filled"`
	Total  uint `json:"total"`
}

func (t *Task) SlotsAvailable() uint {
	return t.Slots.Total - t.Slots.Filled
}

func GetAllTasks() ([]*Task, error) {
	tasks := []*Task{}
	if db.DB == nil {
		return nil, errors.New(db.NOT_CONNECTED)
	}
	rows, err := db.DB.Query("SELECT * FROM tasks")
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}
	for rows.Next() {
		task := &Task{}
		if err = rows.Scan(&task.Name); err != nil {
			return nil, fmt.Errorf("row scan: %w", err)
		}
		tasks = append(tasks, task)
	}
	return tasks, nil
}

func GetTaskByStatus(status string) ([]*Task, error) {
	tasks := []*Task{}
	if db.DB == nil {
		return nil, errors.New(db.NOT_CONNECTED)
	}
	rows, err := db.DB.Query("SELECT * FROM tasks WHERE status = ?", status)
	if err != nil {
		return nil, fmt.Errorf("query: %w", err)
	}
	for rows.Next() {
		task := &Task{}
		if err = rows.Scan(&task.Name); err != nil {
			return nil, fmt.Errorf("row scan: %w", err)
		}
		tasks = append(tasks, task)
	}
	return tasks, nil
}

func AddTask(t *Task) error {
	if db.DB == nil {
		return errors.New(db.NOT_CONNECTED)
	}
	_, err := db.DB.Exec(
		`INSERT INTO tasks (id, name, status, organisation, assignee) VALUES (?, ?, ?, ?, ?)`,
		t.ID,
		t.Name,
		t.Status,
		t.Organisation.ID,
		t.Assignee,
	)
	return err
}

func ModifyTask(taskID string, assigneeID string) error {
	if db.DB == nil {
		return errors.New(db.NOT_CONNECTED)
	}
	_, err := db.DB.Exec(
		`UPDATE tasks SET assignee = ? WHERE id = ?`,
		taskID,
		assigneeID,
	)
	return err
}

func AddOrganisation(o *Organisation) error {
	if db.DB == nil {
		return errors.New(db.NOT_CONNECTED)
	}
	_, err := db.DB.Exec(
		`INSERT INTO organisations VALUES (?, ?)`,
		o.ID,
		o.PictureURL,
	)
	return err
}
