DROP TABLE IF EXISTS tasks;
DROP TABLE IF EXISTS organisations;

CREATE TYPE task_status as ENUM ('Upcoming', 'Ongoing', 'Done');
CREATE TABLE organisations (id text primary key, pictureUrl text);
CREATE TABLE tasks (id text primary key, name text, status task_status, organisation text references organisations(id), assignee text);
CREATE TABLE slots (id text primary key, task text references tasks(id), total integer, filled integer);
