package db

import (
	"database/sql"

	_ "github.com/jackc/pgx/stdlib"
)

var DB *sql.DB

const NOT_CONNECTED = "Not connected to database"

func Connect(connectionString string) (err error) {
	DB, err = sql.Open("pgx", connectionString)
	return err
}
