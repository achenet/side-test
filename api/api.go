package api

import (
	"errors"
	"net/http"
	"side-test/models"

	"github.com/gin-gonic/gin"
)

func NewAPI() *gin.Engine {
	r := gin.Default()

	r.GET("/tasks", GetTasks)
	r.PATCH("/task", ModifyTask)
	r.POST("/tasks", AddTasks)

	return r
}

func GetTasks(c *gin.Context) {
	var tasks []*models.Task
	var err error
	status := c.Query("status")
	if valid(status) {
		tasks, err = models.GetTaskByStatus(status)
	} else {
		tasks, err = models.GetAllTasks()
	}
	if err != nil {
		c.JSON(
			http.StatusInternalServerError,
			gin.H{"error reading tasks": err.Error()},
		)
		return
	}
	c.JSON(http.StatusOK, tasks)

}

func AddTasks(c *gin.Context) {
	tasks := []*models.Task{}
	if err := c.BindJSON(&tasks); err != nil {
		c.JSON(
			http.StatusInternalServerError,
			gin.H{"error binding JSON": err.Error()},
		)
		return
	}
	var errs error
	for _, t := range tasks {
		if err := models.AddTask(t); err != nil {
			errs = errors.Join(errs, err)
		}
	}
	if errs != nil {
		c.JSON(
			http.StatusInternalServerError,
			gin.H{"error adding tasks": errs.Error()},
		)
		return
	}
	c.JSON(http.StatusOK, gin.H{"success": "tasks added"})
}

type ModifyTaskParams struct {
	AssigneeID string `json:"assigneeId"`
}

func ModifyTask(c *gin.Context) {
	taskID := c.Query("task")
	p := &ModifyTaskParams{}
	if err := c.BindJSON(p); err != nil {
		c.JSON(
			http.StatusInternalServerError,
			gin.H{"error binding JSON": err.Error()},
		)
		return
	}
	if err := models.ModifyTask(taskID, p.AssigneeID); err != nil {
		c.JSON(
			http.StatusInternalServerError,
			gin.H{"error modifying task": err.Error()},
		)
		return
	}
	c.JSON(http.StatusOK, gin.H{"task modified": taskID})
}

func valid(status string) bool {
	return status == models.UPCOMING ||
		status == models.ONGOING ||
		status == models.DONE
}

func AddOrganisations(c *gin.Context) {}
