package main

import (
	"flag"
	"log"
	"side-test/api"
	"side-test/db"
)

const DefaultConnectionString = "postgres://postgres:toto@172.17.0.2:5432"

func main() {
	var connectionString = flag.String(
		"connection-string",
		DefaultConnectionString,
		"connection string for postgres",
	)
	flag.Parse()
	log.Println("using connection string:", *connectionString)
	if err := db.Connect(*connectionString); err != nil {
		log.Fatalf("db connection: %s", err.Error())
	}
	log.Fatal(api.NewAPI().Run())
}
